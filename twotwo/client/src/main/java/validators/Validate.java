package validators;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Validate {

    private static final Log LOGGER = LogFactory.getLog(Validate.class);

    public static int validateInt(String number) {
        int valid = -1;
        try {
            if (number == null) {
                throw new IllegalArgumentException("data must not be null");
            }
            valid = Integer.parseInt(number);
            if(valid<0){
                throw new IllegalArgumentException("data must not be null");
            }

        } catch (IllegalArgumentException e) {
            LOGGER.warn("invalid data");
        }
        return valid;
    }

    public static double validateDouble(String number) {
        double valid = -1;
        try {
            if (number == null) {
                throw new IllegalArgumentException("data must not be null");
            }
            valid = Double.parseDouble(number);
            if(valid<0){
                throw new IllegalArgumentException("data must not be null");
            }

        } catch (IllegalArgumentException e) {
            LOGGER.warn("invalid data");
        }
        return valid;
    }
}
