import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.FileInputStream;
import java.io.IOException;

public class ClientStart extends Application{
    private static final Log LOGGER = LogFactory.getLog(ClientStart.class);

    public ClientStart() {
    }

    @Override
    public void start(Stage stage) throws IOException {

        FXMLLoader loader = new FXMLLoader();

        String fxmlDocPath = System.getProperty("user.dir");

        fxmlDocPath=fxmlDocPath.replace('\\','/' );
        FileInputStream fxmlStream = new FileInputStream(fxmlDocPath+"/client/src/main/resources/car.fxml");

        AnchorPane root = loader.load(fxmlStream);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Car info");

        stage.show();
    }


    public static void main(String[] args) {

        Application.launch(args);
    }
}
