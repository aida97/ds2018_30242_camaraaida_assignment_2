package controllers;

import compute.Compute;
import entities.Car;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import static validators.Validate.validateDouble;
import static validators.Validate.validateInt;

public class CarController {

    private static final Log LOGGER = LogFactory.getLog(CarController.class);
    @FXML
    private Button btnTax;

    @FXML
    private Button btnPrice;

    @FXML
    private TextField yearField;

    @FXML
    private TextField priceField;

    @FXML
    private TextField engineCapacityField;

    @FXML
    private TextArea result;

    @FXML
    private TextField hostField;

    @FXML
    private void initialize() {
    }

    public CarController() {
    }

    public void computeTax() throws IOException {

        int year = validateInt(yearField.getText());
        double price = validateDouble(priceField.getText());
        int engineCapacity = validateInt(engineCapacityField.getText());
        String host = hostField.getText();
        boolean ok = (year>0) && (price>0) && (engineCapacity>0);

        if(ok ){

            try{

                String name = "Compute";
                Registry registry = LocateRegistry.getRegistry(host);
                Compute compute = (Compute) registry.lookup(name);
                Car car = new Car(year, engineCapacity, price);
                String task = "tax";
                double tax = compute.executeTask(task, car);
                result.setText("Valoarea taxei: "+ tax);

            }catch (Exception e) {
                LOGGER.error("nu sunt bune datele de conectare");
                e.printStackTrace();
            }
        } else {
            result.setText("ati introdus date gresite");
        }
    }

    public void computePrice() throws IOException {

        int year = validateInt(yearField.getText());
        double price = validateDouble(priceField.getText());
        int engineCapacity = validateInt(engineCapacityField.getText());
        String host = hostField.getText();
        boolean ok = (year > 0) && (price > 0) && (engineCapacity > 0);

        if(ok){

            try{
                String name = "Compute";
                Registry registry = LocateRegistry.getRegistry(host);
                Compute compute = (Compute) registry.lookup(name);
                Car car = new Car(year, engineCapacity, price);
                double priceResult = compute.executeTask("price", car);
                result.setText("Pret: "+ priceResult);

            }catch (Exception e) {
                LOGGER.error("",e);
            }
        } else {
            result.setText("ati introdus date gresite");
        }
    }

}
