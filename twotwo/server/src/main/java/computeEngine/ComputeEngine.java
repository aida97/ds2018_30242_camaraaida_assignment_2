package computeEngine;

import compute.Compute;
import entities.Car;
import tasks.PriceService;
import tasks.TaxService;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class ComputeEngine implements Compute {

    public ComputeEngine() {
        super();
    }

    public Double executeTask(String task, Car car) throws RemoteException {
            if(task.equals("tax")) {
                return new TaxService(car).execute();
            } else if(task.equals("price")) {

                return new PriceService(car).execute();
            }
        throw new RemoteException("invalid task");
    }

    public static void main(String[] args) {

        try {
            String name = "Compute";
            Compute engine = new ComputeEngine();
            Compute stub =
                    (Compute) UnicastRemoteObject.exportObject(engine, 8889);
            LocateRegistry.createRegistry(1099);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(name, stub);
            System.out.println("ComputeEngine bound");
        } catch (Exception e) {
            System.err.println("ComputeEngine exception:");
            e.printStackTrace();
        }
    }

}
