package tasks;


import entities.Car;

import java.io.Serializable;

public class PriceService implements Serializable {
    private Car car;
    public PriceService(Car c){
        car = c;
    }
    private double computePrice(Car c) {
        double result = 0;
        if((2018 - c.getYear()) < 7){
            result = c.getPrice() - ((c.getPrice() / 7) * (2018 - c.getYear()));
        }
        return result;
    }

    public Double execute() {
        if(car == null){
            return 0.0;
        }
        return computePrice(car);
    }
}
