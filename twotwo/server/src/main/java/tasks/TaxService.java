package tasks;


import entities.Car;

import java.io.Serializable;

public class TaxService implements Serializable{
    private Car car;

    public TaxService(Car c){
        car = c;
    }
    public double computeTax(Car c) {
        // Dummy formula
        if (c.getEngineCapacity() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if(c.getEngineCapacity() > 1601) sum = 18;
        if(c.getEngineCapacity() > 2001) sum = 72;
        if(c.getEngineCapacity() > 2601) sum = 144;
        if(c.getEngineCapacity() > 3001) sum = 290;
        return c.getEngineCapacity() / 200.0 * sum;
    }

    public Double execute() {
        if(car == null){
            return 0.0;
        }
        return computeTax(car);
    }
}
