package entities;


import java.io.Serializable;

public class Car implements Serializable{
    private static final long serialVersionUID = 1L;
    private int year;
    private int engineCapacity;

    private double price;

    public Car() {
    }

    public Car(int year, int engineCapacity) {
        this.year = year;
        this.engineCapacity = engineCapacity;
    }

    public Car(int year, int engineCapacity, double price) {
        this.year = year;
        this.engineCapacity = engineCapacity;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(int engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + engineCapacity;
        result = prime * result + year;
        return result;
    }
}
