package compute;

import entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Compute extends Remote{
    <T> T executeTask(String task, Car car) throws RemoteException;
}
